﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed = 0.4f;
    Vector2 destination = Vector2.zero;
    Vector2 direction = Vector2.zero;
    Vector2 nextDirection = Vector2.zero;

    public AudioSource gameOver;
    public AudioSource eatGhostSoundEffect;

    [Serializable]
    public class PointSprites
    {
        public GameObject[] pointSprites;
    }

    public PointSprites points;

    public static int killstreak = 0;

    // script handles
    private GUINav GUINav;
    private GameManager gameManager;
    private ScoreManager scoreManager;

    private bool playingDead = false;

    
    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        scoreManager = GameObject.Find("Game Manager").GetComponent<ScoreManager>();
        GUINav = GameObject.Find("UI Manager").GetComponent<GUINav>();
        destination = transform.position;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (GameManager.gameState)
        {

            case GameManager.GameState.Game:
                ReadInputAndMove();
                Animate();
                
                break;
                

            case GameManager.GameState.Dead:
                if (!playingDead)
                    StartCoroutine("PlayDeadAnimation");
                    gameOver.Stop();
                break;
        }


    }
    //dead animation is played for when pacman loses a life
    IEnumerator PlayDeadAnimation()
    {
        playingDead = true;
        GetComponent<Animator>().SetBool("Die", true);
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().SetBool("Die", false);
        playingDead = false;

        if (GameManager.lives <= 0)
        {
            //freezing animations once lives are gone
            Time.timeScale = 0f; // stop the animations
        }

        else
        {
            gameManager.ResetScene();
            gameOver.Play();
        }
    }
    //animate the chars
    void Animate()
    {
        Vector2 dir = destination - (Vector2)transform.position;
        GetComponent<Animator>().SetFloat("DirX", dir.x);
        GetComponent<Animator>().SetFloat("DirY", dir.y);
    }

    bool Valid(Vector2 direction)
    {
        // cast line from 'next to pacman' to pacman
        // not from directly the center of next tile but just a little further from center of next tile
        Vector2 pos = transform.position;
        direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
        RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
        return hit.collider.name == "pacdot" || (hit.collider == GetComponent<Collider2D>());
    }
    //reset the destination
    public void ResetDestination()
    {
        destination = new Vector2(15f, 11f);
        GetComponent<Animator>().SetFloat("DirX", 1);
        GetComponent<Animator>().SetFloat("DirY", 0);
    }
    //read users input from the keyboard 
    void ReadInputAndMove()
    {
        // move closer to destination
        Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);

        // get the next direction from keyboard
        if (Input.GetAxis("Horizontal") > 0) nextDirection = Vector2.right;
        if (Input.GetAxis("Horizontal") < 0) nextDirection = -Vector2.right;
        if (Input.GetAxis("Vertical") > 0) nextDirection = Vector2.up;
        if (Input.GetAxis("Vertical") < 0) nextDirection = -Vector2.up;

        // if pacman is in the center of a tile
        if (Vector2.Distance(destination, transform.position) < 0.00001f)
        {
            if (Valid(nextDirection))
            {
                destination = (Vector2)transform.position + nextDirection;
                direction = nextDirection;
            }
            else   // if next direction is not valid
            {
                if (Valid(direction))  // and the prev. direction is valid
                    destination = (Vector2)transform.position + direction;   // continue on that direction

                // otherwise, do nothing
            }
        }
    }
    //get current direction
    public Vector2 getDir()
    {
        return direction;
    }
    //update score for when pacman gets a ghost kill
    public void UpdateScore()
    {
        killstreak++;
        eatGhostSoundEffect.Play();

        // limit killstreak at 4
        if (killstreak > 4) killstreak = 4;

        Instantiate(points.pointSprites[killstreak - 1], transform.position, Quaternion.identity);
        GameManager.score += (int)Mathf.Pow(2, killstreak) * 100;

    }
}
